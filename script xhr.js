/*
Задание
Получить список всех планет из серии фильмов Звездные войны, и вывести на экран список персонажей, 
для которых эта планета - родной мир.

Технические требования:

Отправить AJAX запрос по адресу https://swapi.co/api/planets/ и получить список всех планет серии фильмов Звездные войны.
Для каждой планеты получить с сервера список персонажей, для которых она является родным миром. 
Список персонажей можно получить из свойства residents.
Как только с сервера будет получена информация о планетах, сразу же вывести список всех планет на экран. 
Необходимо указать имя планеты, климат, а также тип преобладающей местности (поля name, climate и terrain).
Как только с сервера будет получена информация о персонажах, родившихся на какой-то планете, вывести эту 
    информацию на страницу под названием планеты.
Необходимо написать два варианта реализации в разных .js файлах. Один - с помощью fetch, другой - с помощью promise.
*/

const   requestURL = 'https://swapi.dev/api/planets/',
        xhr = new XMLHttpRequest(),
        tBody = document.querySelector('tbody'),
        create = value => document.createElement(value); //create функция

xhr.open('GET', requestURL);

xhr.send();  

xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) { // если получен ответ
        if (xhr.status == 200) { // и если статус код ответа 200
            let responseText = xhr.responseText; // responseText - текст ответа полученного с сервера.
            let response = JSON.parse(responseText); //переобразовуем в Json
            starWars(response);
        }
    }
}

async function starWars(response){
    let swPlanets = response.results;
    //создаём header таблицы 
    const   tr = create('tr'),
            tdName = create('th'),
            tdClimate = create('th'),
            tdTerrain = create('th');

    tdName.innerText = 'name';
    tdClimate.innerText = 'climate';
    tdTerrain.innerText = 'terrain';

    tBody.append(tr);
    tr.append(tdName,tdClimate,tdTerrain);

    // перебираем все планети и выводим в таблицу
    swPlanets.forEach(element => {
        const   planetTr = create('tr'),
                planetTdName = create('td'),    
                planetTdClimate = create('td'),    
                planetTdTerrain = create('td'),
                planetName = element.name,
                planetClimate = element.climate,
                planetRettain = element.terrain,
                [...residentsArr] = element.residents;

        planetTdName.innerText = planetName;
        planetTdClimate.innerText = planetClimate;
        planetTdTerrain.innerText = planetRettain;
                        
        tBody.append(planetTr);
        planetTr.append(planetTdName, planetTdClimate, planetTdTerrain); 
        planetTr.classList.add('planetTr');

        residentsArr.forEach(el =>{
            //запрос персонажа
              function residentRequestF(){
                let xhrRes = new XMLHttpRequest();
                xhrRes.open('GET', el);

                xhrRes.send();  
                
                xhrRes.onreadystatechange = function () {
                    if (xhrRes.readyState == 4) { // если получен ответ
                        if (xhrRes.status == 200) { // и если статус код ответа 200
                            const responseTextRes = xhrRes.responseText, // responseText - текст ответа полученного с сервера.
                                responseRes = JSON.parse(responseTextRes), //переобразовуем в Json
                                planetResidentsTr = create('tr'),
                                planetResidents = create('td'),
                                residentsAtt = document.createAttribute('colspan');
                                
                        residentsAtt.value = '3';
                        planetResidents.setAttributeNode(residentsAtt);
                        planetResidents.innerText = responseRes.name;  

                        planetTr.after(planetResidentsTr);
                        planetResidentsTr.append(planetResidents);
                        planetResidentsTr.classList.add('trForResidents');

                        }
                    }
                } 
            }
            residentRequestF();
        });
    });

}  


